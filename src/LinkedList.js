/*
 * Implementation of a doubly linked list.
 */

class LinkedList {
    constructor(array = []) {
        this.head = LinkedList.toLinkedList(array);
    }

    // Gets the value of the element at index `i`
    get(i) {
        return this.idx(i).get();
    }

    // Navigates to a certain index of the list
    idx(i) {
        if (i < 0)
            throw new ReferenceError("Index cannot be negative.");
        else
            return this.head.goto(i);
    }

    size() {
        var siz = 0;
        this.forEach(elem => siz++);
        return siz;
    }

    // Applies a function to each element of the linked list
    forEach(fn) {
        var cur = this.head, idx = 0;
        while (cur.hasNext()) {
            cur = cur.getNext();
            idx++;
            fn(cur, idx);
        }
    }

    f(predicate) {
        var cur = this.head, idx = 0;
        while (cur.hasNext()) {
            cur = cur.getNext();
            idx++;
            if (predicate(cur.get(), idx))
                return {idx: idx, val: cur.get()};
        }

        return false;
    }

    // Returns the first element for which the predicate returns true.
    find(predicate) {
        const res = this.f(predicate);
        if (!res)
            return null;
        else
            return res.val;
    }

    // Returns the index of the first element for which the predicate returns true.
    findIndex(predicate) {
        const res = this.f(predicate);
        if (!res)
            return -1;
        else
            return res.idx;
    }

    tojson() {
        // collect the list
        var curr = this.head;
        var acc = [];

        while (curr.hasNext()) {
            curr = curr.getNext();
            acc.push(curr.tojson());
        }

        return acc;
    }

    static fromjson(array) {
        return new LinkedList(array);
    }

    static toLinkedList(array) {
        const head = new ListElement();
        var prev = head;
        array.forEach(val => prev = prev.insertAfter(val));
        return head;
    }
}

class ListElement {
    constructor(val = null, previous = null, next = null) {
        this.val = val;
        this.previous = previous;
        this.next = next;
    }

    get() {
        return this.val;
    }

    // Internal navigation through the linked list
    goto(idx) {
        var curr = this;
        while (idx > 0) {
            if (curr.next === null) {
                throw new ReferenceError("Index out of range.");
            }
            
            curr = curr.next;
            idx--;
        }

        return curr;
    }

    insertAfter(elem) {
        const listElem = new ListElement(elem);

        // Add `listElem` between ourself and the next element from the list
        const nextTmp = this.next;

        // Point to `listElem` (this --> listElem) and let `listElem` point to us (this <-- listElem)
        this.setNext(listElem);
        listElem.setPrevious(this);

        // Let `listElem` point to `nextTmp` (listElem --> nextTmp) and `nextTmp` point to `listElem` (listElem <-- nextTmp)
        // We could be the last element of the list, in which case `nextTmp` is null.
        if (nextTmp) {
            listElem.setNext(nextTmp);
            nextTmp.setPrevious(listElem);
        }

        return listElem;
    }

    // Delete an element
    delete() {
        // cannot delete the head pointer
        if (!this.previous)
            throw new Error("Cannot delete the head pointer of a linked list (i.e. cannot delete index 0).");
        else {
            // Make our previous element point to our next element
            this.previous.setNext(this.getNext()); // will make the previous point to null if we are the last element

            // Let our next element point to our previous element
            if (this.hasNext()) {
                this.next.setPrevious(this.previous);
            }
        }
    }

    setPrevious(prev) {
        this.previous = prev;
    }

    setNext(next) {
        this.next = next;
    }

    hasNext() {
        return this.next !== null;
    }

    getNext() {
        return this.next;
    }

    getPrevious() {
        return this.previous;
    }

    tojson() {
        return this.val;
    }
}

module.exports = LinkedList;
