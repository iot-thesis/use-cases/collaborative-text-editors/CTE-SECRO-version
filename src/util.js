var express = require('express');
var path    = require('path');
var app     = express();
var ip      = require('ip');

/*
 * Serve the text editor as a static file.
 */

app.use(express.static(path.join(__dirname)));
app.use('/images', express.static(path.join(__dirname, '..', 'images')));

/*
 * Initialize websocket
 */

var WebSocketServer = require('ws').Server,
    port = 8080,
    wss = new WebSocketServer({port: port});

console.log('Websocket server listening at http://127.0.0.1:' + port);

/*
 * Listen for connections and messages
 */

var socket;
var buffer = [];
wss.on('connection', function(sock) {
    socket = sock;
    onSocket(sock);
    socket.on('message', function(msg) {
        msg = JSON.parse(msg);
        if (!msg.operation || !msg.args)
            console.log('Did not understand message coming from the front-end.');
        else
            onMessage(msg);
    });

    socket.on('close', function() {
        socket = null;
    });

    socket.on('error', function(error) {
        console.log('Socket error: ' + error);
        socket = null;
    });
});

function onSocket(s) {
    socket = s;
    flushBuffer();
}

function flushBuffer() {
    buffer.forEach(sendMessage);
    buffer = [];
}

// Pushes the new document content to the front-end
function pushUpdate(editor) {
    var msg = { type: 'documentUpdate', content: editor.getRawContent() };
    sendMessage(msg);
}

/*
 * Processes received messages
 */

function onMessage({operation, args}) {
    switch (operation) {
        case 'insertAfter':
            var [previousCharacterID, addedCharacter] = args;
            var newCharacter = new Character(addedCharacter);
            editorService.textEditor.insertAfter(previousCharacterID, newCharacter);
            break;
        case 'delete':
            var [characterID] = args;
            editorService.textEditor.delete(characterID);
            break;
        default:
            console.log('Unsupported operation \'' + operation + '\'');
    }
}

function sendMessage(msg) {
    if (socket)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}

function setEditorService(service, character) {
    editorService = service;
    Character = character;
}

module.exports.pushUpdate = pushUpdate;
module.exports.init = setEditorService;