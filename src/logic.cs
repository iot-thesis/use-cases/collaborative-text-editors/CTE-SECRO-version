const LinkedList = require('./LinkedList');

/*
 * Implementation of a collaborative text editor as a JSON CRDT.
 * A text document is a sequence of characters.
 *
 * Supported operations:
 *  - insertAfter(characID, charac) --> characID
 *  - delete(characID) --> void
 *  - clearDocument()  --> void
 *  - getContent() --> string
 */

// Representation of a character in a text document
var characterCounter = 1;
class Character {
    constructor(c, id) {
        if (c.length !== 1) { throw new TypeError('Expected a character, but got a string with a length that is not 1.'); }
        this.char    = c;
        this.id      = id ? id : (ipAddress + ':' + characterCounter++);
    }
}

// TextEditor SECRO
class TextEditor {
    constructor() {
        this._content = new LinkedList();
    }
    
    insertAfter(characterID, character) {
        character = JSON.parse(JSON.stringify(character)); // Because the received arguments are immutable!
        if (!characterID) {
            // Insert at the beginning of the document
            this._content.idx(0).insertAfter(character);
        }
        else {
            if (this.getCharacter(character.id)) {
                throw new Error('Characters must be unique, cannot insert the same character multiple times.');
            }
            
            var idx = this.getCharacterIndex(characterID);
            if (idx != -1) {
                // Found
                this._content.idx(idx).insertAfter(character);
            }
            else {
                throw new Error('Cannot insert character ' + character.id + ' after an unexisting character ' + characterID + '.');
            }
        }

        return character.id;
    }

    pre insertAfter(state, characterID, character) {
        // The character after which to insert must exist
        return characterID === null ||
               state.getCharacterIndex(characterID) !== -1;
    }
    
    post insertAfter(state, originalState, args, res) {
        /*
         * The character we inserted must occur after the character it was inteded to be.
         * It must not occur right after it since concurrent insertions after the same element are possible.
         */
        const [characterID, character] = args;
        return state.getCharacterIndex(characterID) < state.getCharacterIndex(character.id);
    }
    
    delete(characterID) {
        var idx = this.getCharacterIndex(characterID);
        if (idx != -1) {
            // Found
            this._content.idx(idx).delete();
        }
    }
    
    post delete(state, originalState, args, res) {
        const [characterID] = args;
        return state.getCharacterIndex(characterID) === -1;
    }

    @accessor
    getSize() {
        return this._content.size();
    }

    @accessor
    getContent() {
        var content = "";
        this._content.forEach(char => {
            char = char.get();
            content += char.char;
        });
        return content;
    }

    @accessor
    getRawContent() {
        var content = [];
        this._content.forEach(char => {
            char = char.get();
            content.push(char);
        });
        return content;
    }

    @accessor
    getCharacterIndex(id) {
        return this._content.findIndex(c => c.id === id);
    }

    @accessor
    getCharacter(id) {
        return this._content.find(c => c.id === id);
    }
    
    tojson() {
        return this._content;
    }
    
    static fromjson(content) {
        var editor = new TextEditor();
        editor._content = content;
        return editor;
    }
}

Factory.registerCmRDTClass(TextEditor);
Factory.registerExchangeableClass(LinkedList);

module.exports.Character  = Character;
module.exports.TextEditor = TextEditor;